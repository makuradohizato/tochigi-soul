from typing import Any
import discord
from discord import Message
import requests
import os
import re
from time import sleep
from os.path import join, dirname
from dotenv import load_dotenv

# 環境変数の設定
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)
TOKEN = os.environ.get('TOKEN')
API_URL = os.environ.get("API_URL")

client = discord.Client()

cmd_pattern = re.compile(r'S?([+\-(]*\d+|\d+B\d+|C[+\-(]*\d+|choice|D66|(repeat|rep|x)\d+|\d+R\d+|\d+U\d+|BCDiceVersion|CCB?|RESB?|CBRB?)')

# メッセージに対して発生するイベント
@client.event
async def on_message(message: Message):
    content = message.content
    # 発言者がbotの場合は無視
    author: Any = message.author
    if author.bot:
        return

    # コマンド形式が合ってれば実行
    cmd = cmd_pattern.search(content)
    if cmd:
        res = requests.get(f'{API_URL}{content}').json()
        await message.channel.send(res['text'])

client.run(TOKEN)
